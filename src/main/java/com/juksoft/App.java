package com.juksoft;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            throw new IllegalArgumentException("No directory given to index.");
        }
        final File indexableDirectory = new File(args[0]);
        File[] files = indexableDirectory.listFiles();
        if (files != null) {
            System.out.printf("%d files read in directory %s", files.length, args[0]);
        } else {
            throw new Exception("No such directory at path: " + args[0]);
        }

        Map<String, Set<String>> map = new HashMap<>();
        for (File f : files) {
            map.put(f.getPath(), getWordsFromFile(f));
        }
        Scanner scanner = new Scanner(System.in);

        while (true) {
            Set<String> split = getArrayOfSearchedWords(scanner);
            Map<String, Double> rankForFile = new HashMap<>();
            for (Map.Entry<String, Set<String>> entry : map.entrySet()) {
                rankForFile.put(entry.getKey(), getRank(split, entry));
            }
            rankForFile.entrySet().stream().limit(10)
                .sorted(Comparator.comparing(Entry::getValue))
                .forEach(e -> System.out.println(e.getKey() + " " + e.getValue()));
        }
    }

    private static Set<String> getArrayOfSearchedWords(Scanner scanner) {
        System.out.println("saarch> ");
        String s = scanner.nextLine();
        return new HashSet<>(Arrays.asList(s.split(" ")));
    }

    private static double getRank(Set<String> split, Entry<String, Set<String>> entry) {
        int wordCounter = 0;
        for (String word : split) {
            if (entry.getValue().contains(word)) {
                wordCounter++;
            }
        }
        return ((double) wordCounter / split.size()) * 100.0;
    }

    private static Set<String> getWordsFromFile(File f) throws IOException {
        Set<String> wordsFromFile = new HashSet<>();
        Stream<String> lines = Files.lines(Paths.get(f.getPath()));
        lines.forEach(line -> {
            String[] split = line.split(" ");
            wordsFromFile.addAll(Arrays.asList(split));
        });
        return wordsFromFile;
    }
}
